
public class Point {
	private double x, y;
	private String name;
	
	public Point(double x, double y, String name) {
		this.x = x;
		this.y = y;
		this.name = name;
	}
	
	public double getX() {
		return x;
	}
	
	public double getY() {
		return y;
	}
	
	public String getName() {
		return name;
	}
}
