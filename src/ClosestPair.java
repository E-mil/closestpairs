import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Scanner;

public class ClosestPair {

	ArrayList<Point> xPoints/* , yPoints */;
	private static int nbrPoints = 0;
	private static Point points[];

	public ClosestPair(int nbrPoints) {
		xPoints = new ArrayList<Point>(nbrPoints);
		// yPoints = new ArrayList<Point>(nbrPoints);
		points = new Point[2];
	}

	public void addPoints(Scanner sc) {
		while (sc.hasNextLine()) {
			String temp = sc.nextLine().trim();
			if (temp.equals("EOF") || temp.equals(""))
				break;
			int index = temp.indexOf(" ");
			String name = temp.substring(0, index);
			temp = temp.substring(index + 1).trim();
			index = temp.indexOf(" ");
			Point p = new Point(Double.parseDouble(temp.substring(0, index).trim()),
					Double.parseDouble(temp.substring(index + 1).trim()), name);
			xPoints.add(p);
			// yPoints.add(p);
		}
		nbrPoints = xPoints.size();
	}

	public void addPoints() {
		Scanner sc = new Scanner(System.in);
		String temp = sc.nextLine().trim();
		for (int i = 0; i < Integer.parseInt(temp); i++) {
			String temp2 = sc.nextLine().trim();
			int index = temp2.indexOf(" ");
			Point p = new Point(Double.parseDouble(temp2.substring(0, index)),
					Double.parseDouble(temp2.substring(index + 1)), i + "");
			xPoints.add(p);
			// yPoints.add(p);
		}
		sc.close();
	}

	public double execute() {
		// yPoints.sort(new Comparator<Point>() {
		// @Override
		// public int compare(Point o1, Point o2) {
		// if (o1.getY() < o2.getY())
		// return -1;
		// else if (o1.getY() > o2.getY())
		// return 1;
		// return 0;
		// }
		// });
		xPoints.sort(new Comparator<Point>() {
			@Override
			public int compare(Point o1, Point o2) {
				if (o1.getX() < o2.getX())
					return -1;
				else if (o1.getX() > o2.getX())
					return 1;
				return 0;
			}
		});
		return findMinDistance(0, xPoints.size());
	}

	private double findMinDistance(int index, int size) {
		if (size == 2) {
			Point a = xPoints.get(index);
			Point b = xPoints.get(index + 1);
			points[0] = a;
			points[1] = b;
			return Math.sqrt(Math.pow(a.getX() - b.getX(), 2) + Math.pow(a.getY() - b.getY(), 2));
		} else if (size == 3) {
			Point a = xPoints.get(index);
			Point b = xPoints.get(index + 1);
			Point c = xPoints.get(index + 2);
			points[0] = null;
			points[1] = null;
			double ax = a.getX();
			double ay = a.getY();
			double bx = b.getX();
			double by = b.getY();
			double cx = c.getX();
			double cy = c.getY();
			return Math.min(
					Math.min(Math.sqrt((ax - bx) * (ax - bx) + (ay - by) * (ay - by)),
							Math.sqrt((ax - cx) * (ax - cx) + (ay - cy) * (ay - cy))),
					Math.sqrt((cx - bx) * (cx - bx) + (cy - by) * (cy - by)));
		}
		double l = findMinDistance(index, size / 2);
		double r = findMinDistance(index + size / 2, (size + 1) / 2);
		double min = Math.min(l, r);
		ArrayList<Point> temp = new ArrayList<Point>();
		for (int i = index; i < size; i++) {
			temp.add(xPoints.get(i));
		}
		return Math.min(min, findMinDistanceOverMiddle(index, size, min, temp));
	}

	private double findMinDistanceOverMiddle(int index, int size, double min, ArrayList<Point> temp) {
		int middle = index + size / 2;
		ArrayList<Point> temp2 = new ArrayList<Point>();
		double p = xPoints.get(middle).getX();
		for (Point yp : temp) {
			if (Math.abs(yp.getX() - p) < min) {
				temp2.add(yp);
			}
		}
		temp2.sort(new Comparator<Point>() {

			@Override
			public int compare(Point o1, Point o2) {
				if (o1.getY() < o2.getY())
					return -1;
				if (o2.getY() < o1.getY())
					return 1;
				return 0;
			}
		});
		double minOverMiddle = Double.MAX_VALUE;
		for (int i = 0; i < temp2.size(); i++) {
			Point p1 = temp2.get(i);
			for (int j = 1; j < 15; j++) {
				if (i + j >= temp2.size())
					break;
				Point p2 = temp2.get(i + j);
				double p1x = p1.getX();
				double p1y = p1.getY();
				double p2x = p2.getX();
				double p2y = p2.getY();
				double distance = Math.sqrt((p1x - p2x) * (p1x - p2x) + (p1y - p2y) * (p1y - p2y));
				if (distance < minOverMiddle) {
					points[0] = p1;
					points[1] = p2;
					minOverMiddle = distance;
				}
			}
		}
		return minOverMiddle;
	}

	public static void main(String[] args) {
//		File[] files = new File("data/").listFiles();
//		StringBuilder sb = new StringBuilder();
//
//		try {
//			File f = new File("data/a280-tsp.txt");
//			boolean first = true;
//			for (File f : files) {
//				System.out.println(f.getPath());
//				if (first)
//					first = false;
//				else
//					sb.append("\n");
//				Scanner sc = new Scanner(f);
//
//				String temp = sc.nextLine();
//				while (!temp.contains("DIMENSION"))
//					temp = sc.nextLine();
//				int nbrOfPoints = Integer.parseInt(temp.substring(temp.indexOf(":") + 1).trim());
//				temp = sc.nextLine();
//				while (!temp.contains("NODE_COORD_SECTION"))
//					temp = sc.nextLine();


				 ClosestPair cp = new ClosestPair(10);
				 cp.addPoints();
				 double min = cp.execute();
				 System.out.println(min);

//				ClosestPair cp = new ClosestPair(10);
//				cp.addPoints(sc);
//				double min = cp.execute();
//				System.out.println(min);
//
//				sb.append("../" + f.getPath().replace("\\", "/") + ": " + nbrPoints + " " + min);
//				sc.close();
//			}
//			FileWriter fw = new FileWriter(new File("Output.txt"));
//			fw.write(sb.toString());
//			fw.close();
//		} catch (FileNotFoundException e) {
//			e.printStackTrace();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
	}
}
